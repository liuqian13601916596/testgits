package com.ljs.testgits.bean;

import lombok.Data;
import lombok.ToString;

/**
 * 这个是2.0的
 */
@Data
@ToString
public class User {
    private  int id;
    private  String name;
}
