package com.ljs.testgits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestgitsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestgitsApplication.class, args);
    }

}
